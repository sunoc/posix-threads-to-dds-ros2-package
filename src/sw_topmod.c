#include "systembuilder.h"
//#define PROFILER
//#define MPROFILER

void
sw_topmod(void) {
  unsigned int i;
  unsigned int tmp;
  int pass;
  SYSUTM  sysutm_s, sysutm_e;

  lprintf("sw_topmod : Start!");

  while(1)
    {
      lprintf("Press any key to start.");
      lgetchar();

#ifdef PROFILER
      profiler_init(64000);
#endif /* PROFILER */
#ifdef MPROFILER
      mem_profiler_start_cycle(1, 16000);
#endif /* MPROFILER */

      pass = 1;
      for(i = 0; i < 0xf; i++)
	{
	  lprintf("sw_topmod : send = 0x%x", i);

	  get_utm(&sysutm_s);
	  SW_SW_BC_WRITE(i);
	  SW_SW_BC2_READ(&tmp);
	  get_utm(&sysutm_e);

	  lprintf("sw_topmod : recive = 0x%x, time = %dus", tmp, (unsigned int)(sysutm_e - sysutm_s));

	  assert(tmp == i + 2);
	}

      assert(pass == 1);

#ifdef MPROFILER
      mem_profiler_out_cycle();
#endif /* MPROFILER */
#ifdef PROFILER
      profiler_out();
#endif /* PROFILER */
    }
}
