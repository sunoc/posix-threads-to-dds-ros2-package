#include "systembuilder.h"

void
hw_rcvmod(void){
	unsigned int a;
	unsigned char b;

	lprintf("hw_rcvmod : Start!");

	while(1){
		SW_HW_BC_READ(&a);

		b = a + 1;

		HW_HW_BC_WRITE(b);
	}
}

