#include "nodes.h"

/*----------------------------------------------------------------------------*/
int
main(int argc, char * argv[])
{
  rclcpp::init(argc, argv);

  rclcpp::Node::SharedPtr ETP_node =
    std::make_shared<MiniPublisher>("ETP",
				    "extra_test_publisher",
				    "hey, listen!");
  rclcpp::Node::SharedPtr TP_node =
    std::make_shared<MiniPublisher>("TP",
				    "test_publisher",
				    "hello world");
  rclcpp::Node::SharedPtr TS_node =
    std::make_shared<MiniSubscriber>("TS",
				     "test_subscriber",
				     "hello world");
  rclcpp::Node::SharedPtr ETS_node =
    std::make_shared<MiniSubscriber>("ETS",
				     "extra_test_subscriber",
				     "extrapublisher");

  rclcpp::executors::MultiThreadedExecutor executor;
  executor.add_node(TS_node);
  executor.add_node(TP_node);
  executor.add_node(ETS_node);
  executor.add_node(ETP_node);

  executor.spin();

  printf("Shutting down ROS2 nodes gracefully...\n");
  rclcpp::shutdown();
  return 0;
}
