#include "nodes.h"

/*
 *  ****************************************************************************
 *  ROS2 nodes parent class function definitions
 */

/* ---------------------------------------------------------------------------*/
/* Public functions definitions */
void
MinimalPubSubNode::subscribe_to(const std::string topic,
				const std::string expected)
{
  expected_str_ = expected;
  subscription_ = this->create_subscription\
    <std_msgs::msg::String>(topic,
			    10,
			    std::bind(&MinimalPubSubNode::topic_callback,
				      this,
				      _1));
}

void
MinimalPubSubNode::publish_to(const std::string topic,
			      const std::string to_send)
{
  to_send_str_ = to_send;
  publisher_ = this->create_publisher<std_msgs::msg::String>(topic, 10);
  assert(NULL != publisher_);
  timer_ = this->create_wall_timer(500ms,
				   std::bind(&MinimalPubSubNode::\
					     timer_callback, this));
}

void
MinimalPubSubNode::forward_to(const std::string topic_in,
			      const std::string topic_out)
{
  publisher_ = this->create_publisher<std_msgs::msg::String>(topic_out, 10);
  subscription_ = this->create_subscription	\
    <std_msgs::msg::String>(topic_in,
			    10,
			    std::bind(&MinimalPubSubNode::fw_callback,
				      this,
				      _1));
}

/* ---------------------------------------------------------------------------*/
/* Private functions definitions */
void
MinimalPubSubNode::topic_callback(const std_msgs::msg::String & msg) const
{
  /* The only string I'm suppose to receive on that topic is TS*/
  assert(0 == strcmp(expected_str_.c_str(), msg.data.c_str()));
}

void
MinimalPubSubNode::timer_callback()
{
  auto message = std_msgs::msg::String();
  message.data = to_send_str_.c_str();
  publisher_->publish(message);
}

void
MinimalPubSubNode::fw_callback(const std_msgs::msg::String & msg) const
{
  auto message = std_msgs::msg::String();
  message.data = msg.data.c_str();
  publisher_->publish(message);
}
