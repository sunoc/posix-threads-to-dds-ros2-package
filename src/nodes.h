#ifndef _nodes_h_
#define _nodes_h_
#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

/*
 *  ****************************************************************************
 *  Headers,  global variables and structures
 */
using namespace std::chrono_literals;
using std::placeholders::_1;

/*
 *  ****************************************************************************
 *  ROS2 nodes parent class definition
 */
class MinimalPubSubNode : public rclcpp::Node
{
 public:
 MinimalPubSubNode(const std::string & name) : Node(name)
    {
      RCLCPP_INFO(this->get_logger(), "Parent node class called.");
    }

  void subscribe_to(const std::string topic,  const std::string expected);
  void publish_to(const std::string topic,    const std::string to_send);
  void forward_to(const std::string topic_in, const std::string topic_out);

 private:
  void topic_callback(const std_msgs::msg::String & msg) const;
  void timer_callback();
  void fw_callback(const std_msgs::msg::String & msg) const;

  rclcpp::Subscription<std_msgs::msg::String>::SharedPtr subscription_;
  rclcpp::Publisher<std_msgs::msg::String>::SharedPtr publisher_;
  rclcpp::TimerBase::SharedPtr timer_;
  std::string expected_str_, to_send_str_;
};

/*
 *  ****************************************************************************
 *  Nodes child class definitions
 */
class MiniSubscriber : public MinimalPubSubNode
{
public:
  MiniSubscriber(const std::string topic, const std::string name,
		 const std::string expected) :
    MinimalPubSubNode(name)
  {
    RCLCPP_INFO(this->get_logger(), "%s node created!!", name.c_str());
    this->subscribe_to(topic, expected);
  }
};

class MiniPublisher : public MinimalPubSubNode
{
public:
  MiniPublisher(const std::string topic, const std::string name ,
		const std::string to_send) :
    MinimalPubSubNode(name)
  {
    RCLCPP_INFO(this->get_logger(), "%s node created!!", name.c_str());
    this->publish_to(topic, to_send);
  }
};

class MiniForwarder : public MinimalPubSubNode
{
public:
 MiniForwarder(const std::string topic_in, const std::string topic_out,
	       const std::string name) :
    MinimalPubSubNode(name)
  {
    RCLCPP_INFO(this->get_logger(), "%s node created!!", name.c_str());
    this->forward_to(topic_in, topic_out);
  }
};

#endif /* _nodes_h_ */
