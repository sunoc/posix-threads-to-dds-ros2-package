//////////////////////////////////////////////////////////////////////
// Copyright (c) 2005-2007 Shinya Honda (honda@ertl.jp)
// Copyright (c) 2007-2020 Embedded and Real-Time Systems Laboratory
//        Graduate School of Information Science, Nagoya Univ., JAPAN
//
// sp.h
//
// 2023-11-18 12:06:09.621477
//
// Sysgen Glue Routine Header
//
//////////////////////////////////////////////////////////////////////

#ifndef _sp_h_
#define _sp_h_

/* #define _GNU_SOURCE */

/*
 * POSIX用のインクルードファイル
 */
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <pthread.h>
#include <unistd.h>
#include <errno.h>
#include <sys/timerfd.h>
#include <time.h>
#include <stdint.h>
#include <stdarg.h>
#include <assert.h>

#define Inline	static inline

#define E_ID            (-18)       /* 不正ID番号 */

#define SOFTWARE

/* Enable debug prints */
#define DEBUG 3

Inline void lprintf(const char *format, ...);

#if defined(DEBUG) && DEBUG > 0
#define DEBUG_PRINT(fmt, ...) lprintf("%s():%d:\t " fmt,	\
				      __func__, __LINE__,	\
				      ## __VA_ARGS__)
#else
#define DEBUG_PRINT(fmt, ...) /* Don't do anything in release builds */
#endif

typedef signed int      int_t;      /* 自然なサイズの符号付き整数 */
typedef unsigned int    uint_t;     /* 自然なサイズの符号無し整数 */
typedef int_t           ER;         /* エラーコード */
typedef int_t           ID;         /* オブジェクトのID番号 */

/*
 *  サービスコールのエラーのログ出力
 */
Inline void
svc_perror(const char *file, int_t line, const char *expr, ER ercd)
{
  char buf[1024];
  if (ercd == PTHREAD_BARRIER_SERIAL_THREAD)
    {
      DEBUG_PRINT("Thread barrier is okay, code %d reveiced.", ercd);
    }
  else if (ercd < 0)
    {
      sprintf(buf,"%d reported by `%s' in line %d of `%s'.\n", ercd, expr, line, file);
      perror(buf);
      exit(1);
    }
}

#define	SVC_PERROR(expr)    svc_perror(__FILE__, __LINE__, #expr, (expr))

#define SHARED_VAR

#define delay(tim)  SVC_PERROR(usleep((tim)*1000))

#define lgetchar getchar

Inline void
lprintf(const char *format, ...)
{
    char buffer[1024];
    va_list ap;

    va_start(ap, format);
    vsprintf(buffer, format, ap);
    va_end(ap);

    printf("%s\n", buffer);
}

typedef u_int64_t SYSUTM;

Inline void
get_utm(SYSUTM *ptime)
{
    struct timespec ts;

    SVC_PERROR(clock_gettime(CLOCK_REALTIME, &ts));
    *ptime = (ts.tv_sec * 1000000) + (ts.tv_nsec / 1000);
}


/*
 *  SWBC関連の定義
 */

/*
 *  SWBCへのデータの送信
 */
extern int32_t WriteSWBC(uint32_t id,  intptr_t data);

/*
 *  SWBCからのデータの送信
 */
extern int32_t ReadSWBC(uint32_t id,  intptr_t *pdata);


Inline int
ftox(float f)
{
    union {
        float f;
        int   i;
    } fi;
    fi.f = f;
    return fi.i;
}

Inline float
xtof(int i){
    union {
        float f;
        int   i;
    } fi;
    fi.i = i;
    return fi.f;
}

/*
 *  BCのための記述
 */
Inline ER
char_ReadSWBC(ID swbcid, uint8_t *pdata){
    intptr_t int_data;
    ER ercd;

    SVC_PERROR(ercd = ReadSWBC(swbcid, (intptr_t*)&int_data));
    *pdata = int_data;

    return ercd;
}

Inline ER
short_ReadSWBC(ID swbcid, uint16_t *pdata){
    intptr_t int_data;
    ER ercd;

    SVC_PERROR(ercd = ReadSWBC(swbcid, (intptr_t*)&int_data));
    *pdata = int_data;

    return ercd;
}

Inline ER
word_ReadSWBC(ID swbcid, uint32_t *pdata){
    ER ercd;
    intptr_t int_data;

    SVC_PERROR(ercd = ReadSWBC(swbcid, (intptr_t*)&int_data));
    *pdata = int_data;

    return ercd;
}

Inline ER
float_ReadSWBC(ID swbcid, float *pdata){
    intptr_t int_data;
    ER ercd;

    SVC_PERROR(ercd = ReadSWBC(swbcid, (intptr_t*)&int_data));
    *pdata = xtof(int_data);

    return ercd;
}

Inline ER
char_WriteSWBC(ID swbcid, uint8_t data){
    int w_data = data;
    ER ercd;

    SVC_PERROR(ercd = WriteSWBC(swbcid, (intptr_t)w_data));

    return ercd;
}

Inline ER
short_WriteSWBC(ID swbcid, uint16_t data){
    int w_data = data;
    ER ercd;

    SVC_PERROR(ercd = WriteSWBC(swbcid, (intptr_t)w_data));

    return ercd;
}

Inline ER
word_WriteSWBC(ID swbcid, uint32_t data){
    ER ercd;

    SVC_PERROR(ercd = WriteSWBC(swbcid, (intptr_t)data));

    return ercd;
}

/*
 *  ここから生成
 */

//For sw_sw_bc : BComPrim
#define SW_SW_BC_WRITE(data)  word_WriteSWBC(SW_SW_BC_BC, (uint32_t)data)
#define SW_SW_BC_PWRITE(data) word_WriteSWBC(SW_SW_BC_BC, (uint32_t)data)
#define SW_SW_BC_READ(pdata)  word_ReadSWBC(SW_SW_BC_BC, (uint32_t*)pdata)
#define SW_SW_BC_PREAD(pdata) word_ReadSWBC(SW_SW_BC_BC, (uint32_t*)pdata)

//For sw_sw_bc2 : BComPrim
#define SW_SW_BC2_WRITE(data)  word_WriteSWBC(SW_SW_BC2_BC, (uint32_t)data)
#define SW_SW_BC2_PWRITE(data) word_WriteSWBC(SW_SW_BC2_BC, (uint32_t)data)
#define SW_SW_BC2_READ(pdata)  word_ReadSWBC(SW_SW_BC2_BC, (uint32_t*)pdata)
#define SW_SW_BC2_PREAD(pdata) word_ReadSWBC(SW_SW_BC2_BC, (uint32_t*)pdata)

//For sw_hw_bc : BComPrim
#define SW_HW_BC_WRITE(data)  word_WriteSWBC(SW_HW_BC_BC, (uint32_t)data)
#define SW_HW_BC_PWRITE(data) word_WriteSWBC(SW_HW_BC_BC, (uint32_t)data)
#define SW_HW_BC_READ(pdata)  word_ReadSWBC(SW_HW_BC_BC, (uint32_t*)pdata)
#define SW_HW_BC_PREAD(pdata) word_ReadSWBC(SW_HW_BC_BC, (uint32_t*)pdata)

//For hw_sw_bc : BComPrim
#define HW_SW_BC_WRITE(data)  word_WriteSWBC(HW_SW_BC_BC, (uint32_t)data)
#define HW_SW_BC_PWRITE(data) word_WriteSWBC(HW_SW_BC_BC, (uint32_t)data)
#define HW_SW_BC_READ(pdata)  word_ReadSWBC(HW_SW_BC_BC, pdata)
#define HW_SW_BC_PREAD(pdata) word_ReadSWBC(HW_SW_BC_BC, pdata)

//For hw_hw_bc : BComPrim
#define HW_HW_BC_WRITE(data)  word_WriteSWBC(HW_HW_BC_BC, (uint32_t)data)
#define HW_HW_BC_PWRITE(data) word_WriteSWBC(HW_HW_BC_BC, (uint32_t)data)
#define HW_HW_BC_READ(pdata)  word_ReadSWBC(HW_HW_BC_BC, pdata)
#define HW_HW_BC_PREAD(pdata) word_ReadSWBC(HW_HW_BC_BC, pdata)

/*
 *  SWBCの数
 */
#define TNUM_SWBC  5

/*
 *  SWBCのID
 */
#define SW_SW_BC_BC     1
#define SW_SW_BC2_BC    2
#define SW_HW_BC_BC     3
#define HW_SW_BC_BC     4
#define HW_HW_BC_BC     5

#endif /* _sp_h_ */
