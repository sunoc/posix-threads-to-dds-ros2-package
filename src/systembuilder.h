//////////////////////////////////////////////////////////////////////
// Copyright (c) 2005-2007 Shinya Honda (honda@ertl.jp)
// Copyright (c) 2007-2020 Embedded and Real-Time Systems Laboratory
//        Graduate School of Information Science, Nagoya Univ., JAPAN
//
// systembuilder.h
//
// 2023-11-18 12:06:09.680219
//
// SystemBuilder Standard Header
//
//////////////////////////////////////////////////////////////////////

#include "systembuilder_def.h"
#include "sp.h"

void sw_topmod(void);
void sw_srmod(void);
void hw_rcvmod(void);
void hw_sndmod(void);

void ros2_sub(void);
void ros2_pub(void);
void ros2_extra_sub(void);
void ros2_extra_pub(void);
