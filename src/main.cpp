#include <chrono>
#include <cstdint>
#include <functional>
#include <memory>
#include <string>
#include <iostream>

#include "nodes.h"

extern "C"
{
#include "systembuilder.h"
#include "sp.h"
}


/*
 *  ****************************************************************************
 *  Defines
 */
#define TNUM_FU 4
#define get_swbccb(swbcid)      (&(SwbccbTable[swbcid - 1]))


typedef void    (*FU)(void);

typedef struct fu_control_block{
  FU        fu;      /* function pointer */
  uint64_t  cyclens; /* period (nsec), 0:no period */
  uint32_t  core;    /* core layout */
  char      *name;   /* duh */
} FCB;

pthread_barrier_t StartBarrier;

FCB FcbTable[TNUM_FU] = {
  {sw_topmod, 0, 0, (char *)"sw_topmod"},
  {sw_srmod,  0, 0, (char *)"sw_srmod" },
  {hw_rcvmod, 0, 0, (char *)"hw_rcvmod"},
  {hw_sndmod, 0, 0, (char *)"hw_sndmod"}
};

typedef struct swbc_initialization_block{
  uint32_t Depth;
} SWBCINIB;

typedef struct {
  pthread_mutex_t Mutex;
  pthread_cond_t RCond;
  pthread_cond_t WCond;
  uint32_t DataCnt;
  uint32_t RIndex;
  uint32_t WIndex;
  int32_t *Buffer;
  const SWBCINIB *pSwBCInib;
} SWBCCB;

extern const SWBCINIB SwbcinibTable[];
extern SWBCCB SwbccbTable[];

SWBCCB SwbccbTable[TNUM_SWBC];

const SWBCINIB SwbcinibTable[TNUM_SWBC] = {
  {1},
  {1},
  {1},
  {1},
  {1}
};


/*
 *  ****************************************************************************
 *  FU thread functions
 */
void *
FuThread(void *arg)
{
  FCB *pfcb = (FCB*)arg;

  DEBUG_PRINT("Threads barrier sync");
  pthread_barrier_wait(&StartBarrier);

  DEBUG_PRINT("Test for cyclens equal zero");
  if (pfcb->cyclens == 0)
    {
      DEBUG_PRINT("Infinite loop for operation");
      while(1)
	{
	  DEBUG_PRINT("function pointer called");
	  (*(pfcb->fu))();
	}
    }
  else
    {
      DEBUG_PRINT("Loop operation init");
      int tfd;
      struct itimerspec its;
      ssize_t r_size;
      u_int64_t t_cnt;

      DEBUG_PRINT("Monotonic clock set");
      tfd = timerfd_create(CLOCK_MONOTONIC,0);
      /* 1回目 */
      its.it_value.tv_sec = pfcb->cyclens / 1000000000;
      its.it_value.tv_nsec = (pfcb->cyclens) %  1000000000;
      /* 1回目以降 */
      its.it_interval.tv_sec = its.it_value.tv_sec ;
      its.it_interval.tv_nsec = its.it_value.tv_nsec;

      timerfd_settime(tfd,0,&its,NULL);
      while(1)
	{
	  r_size = read(tfd,&t_cnt,sizeof(t_cnt));
	  if(r_size == sizeof(t_cnt))
	    {
	      (*(pfcb->fu))();
	    }
	}
    }
}

/*
 *  ****************************************************************************
 *  SWBC init
 */
void
InitSWBC(void)
{
  for (int i = 0; i < TNUM_SWBC; i++)
    {
      SWBCCB *p_swbccb = get_swbccb(i+1);
      p_swbccb->pSwBCInib = &SwbcinibTable[i];
      uint32_t depth = p_swbccb->pSwBCInib->Depth;

      p_swbccb->Buffer = (int32_t *)malloc(depth*sizeof(intptr_t));
      pthread_mutex_init(&(p_swbccb->Mutex), NULL);
      pthread_cond_init(&(p_swbccb->WCond), NULL);
      pthread_cond_init(&(p_swbccb->RCond), NULL);
      p_swbccb->DataCnt = 0;
      p_swbccb->RIndex = 0;
      p_swbccb->WIndex = 0;
    }
}

/*
 *  ****************************************************************************
 *  SWBC read and write functions
 */
int32_t
WriteSWBC(uint32_t id, intptr_t data)
{

  // ID Check
  if ((id == 0)  || (id > TNUM_SWBC))
    return E_ID;

  SWBCCB *p_swbccb = get_swbccb(id);

  pthread_mutex_lock(&(p_swbccb->Mutex));

  while(p_swbccb->DataCnt == p_swbccb->pSwBCInib->Depth)
    {
      pthread_cond_wait(&(p_swbccb->WCond), &(p_swbccb->Mutex));
    }

  p_swbccb->Buffer[p_swbccb->WIndex] = data;
  p_swbccb->DataCnt++;
  p_swbccb->WIndex++;
  p_swbccb->WIndex %= p_swbccb->pSwBCInib->Depth;
  pthread_cond_signal(&(p_swbccb->RCond));

  pthread_mutex_unlock(&(p_swbccb->Mutex));

  return 0;
}

/*----------------------------------------------------------------------------*/
int32_t
ReadSWBC(uint32_t id, intptr_t *pdata)
{
  // ID Check
  if ((id == 0)  || (id > TNUM_SWBC))
    return E_ID;

  SWBCCB *p_swbccb = get_swbccb(id);

  pthread_mutex_lock(&(p_swbccb->Mutex));

  while(p_swbccb->DataCnt == 0)
    {
      pthread_cond_wait(&(p_swbccb->RCond), &(p_swbccb->Mutex));
    }

  *pdata = p_swbccb->Buffer[p_swbccb->RIndex];
  p_swbccb->DataCnt--;
  p_swbccb->RIndex++;
  p_swbccb->RIndex %= p_swbccb->pSwBCInib->Depth;
  pthread_cond_signal(&(p_swbccb->WCond));

  pthread_mutex_unlock(&(p_swbccb->Mutex));

  return 0;
}

/*
 *  ****************************************************************************
 *  ROS2 classes definitions
 */
class MinimalSubscriber : public rclcpp::Node
{
public:
  MinimalSubscriber() : Node("minimal_subscriber"), topic_("topic")
  {
    RCLCPP_INFO(this->get_logger(), "Node created!!");
    subscription_ = this->create_subscription\
      <std_msgs::msg::String>(topic_,
			      10,
			      std::bind(&MinimalSubscriber::topic_callback,
					this,
					_1));
    publisher_ = this->create_publisher<std_msgs::msg::String>("TS", 10);
    timer_ = this->create_wall_timer(1000ms,
				     std::bind(&MinimalSubscriber::\
					       timer_callback, this));
  }

private:
  std::string topic_;

  void
  timer_callback()
  {
    auto message = std_msgs::msg::String();
    message.data = "hello world";
    RCLCPP_INFO(this->get_logger(), "Sending to TB: '%s'", message.data.c_str());
    publisher_->publish(message);
  }

  void
  topic_callback(const std_msgs::msg::String & msg) const
  {
    RCLCPP_INFO(this->get_logger(), "I heard: '%s'", msg.data.c_str());
  }
  rclcpp::Subscription<std_msgs::msg::String>::SharedPtr subscription_;
  rclcpp::TimerBase::SharedPtr timer_;
  rclcpp::Publisher<std_msgs::msg::String>::SharedPtr publisher_;
};

/*----------------------------------------------------------------------------*/
class MinimalPublisher : public rclcpp::Node
{
public:
  MinimalPublisher() : Node("minimal_publisher"), topic_("topic"), count_(0)
  {
    RCLCPP_INFO(this->get_logger(), "Node created!!");
    publisher_ = this->create_publisher<std_msgs::msg::String>(topic_, 10);
    timer_ = this->create_wall_timer(500ms,
				     std::bind(&MinimalPublisher::\
					       timer_callback, this));

    /* Test bench subscription */
    subscription_ = this->create_subscription\
      <std_msgs::msg::String>("TP",
			      10,
			      std::bind(&MinimalPublisher::test_callback,
					this,
					_1));
  }

private:
  std::string topic_;
  size_t count_;

  void
  timer_callback()
  {
    auto message = std_msgs::msg::String();
    message.data = "Hello, world! " + std::to_string(count_++);
    RCLCPP_INFO(this->get_logger(), "Publishing: '%s'", message.data.c_str());
    publisher_->publish(message);
  }

  void
  test_callback(const std_msgs::msg::String & msg) const
  {
    RCLCPP_INFO(this->get_logger(), "From the TB: '%s'", msg.data.c_str());
  }
  rclcpp::Subscription<std_msgs::msg::String>::SharedPtr subscription_;
  rclcpp::TimerBase::SharedPtr timer_;
  rclcpp::Publisher<std_msgs::msg::String>::SharedPtr publisher_;
};

/*----------------------------------------------------------------------------*/
class ExtraSubscriber : public rclcpp::Node
{
public:
  ExtraSubscriber() : Node("extra_subscriber"), topic_("ETP")
  {
    RCLCPP_INFO(this->get_logger(), "Node created!!");
    subscription_ = this->create_subscription\
      <std_msgs::msg::String>(topic_,
			      10,
			      std::bind(&ExtraSubscriber::topic_callback,
					this,
					_1));
  }

private:
  std::string topic_;

  void
  topic_callback(const std_msgs::msg::String & msg) const
  {
    RCLCPP_INFO(this->get_logger(), "I heard: '%s'", msg.data.c_str());
  }
  rclcpp::Subscription<std_msgs::msg::String>::SharedPtr subscription_;
};

/*----------------------------------------------------------------------------*/
class ExtraPublisher : public rclcpp::Node
{
public:
  ExtraPublisher() : Node("extra_publisher"), topic_("ETS")
  {
    RCLCPP_INFO(this->get_logger(), "Node created!!");
    publisher_ = this->create_publisher<std_msgs::msg::String>(topic_, 10);
    timer_ = this->create_wall_timer(500ms,
				     std::bind(&ExtraPublisher::\
					       timer_callback, this));
  }

private:
  std::string topic_;

  void
  timer_callback()
  {
    auto message = std_msgs::msg::String();
    message.data = "extrapublisher";
    RCLCPP_INFO(this->get_logger(), "Publishing: '%s'", message.data.c_str());
    publisher_->publish(message);
  }
  rclcpp::TimerBase::SharedPtr timer_;
  rclcpp::Publisher<std_msgs::msg::String>::SharedPtr publisher_;
};

/*----------------------------------------------------------------------------*/
int
main(int argc, char * argv[])
{
  int_t loop;
  pthread_t th[TNUM_FU];
  cpu_set_t cpuset;

  rclcpp::init(argc, argv);

  rclcpp::Node::SharedPtr ES_node =
    std::make_shared<MiniSubscriber>("EPT",
				     "ExtraSubscriber",
				     "testpayload");
  rclcpp::Node::SharedPtr MP_node =
    std::make_shared<MiniForwarder>("TP",
				    "topic",
				    "MinimalPublisher");
  rclcpp::Node::SharedPtr MS_node =
    std::make_shared<MiniForwarder>("topic",
				    "TS",
				    "MinimalSubscriber");
  rclcpp::Node::SharedPtr EP_node =
    std::make_shared<MiniPublisher>("ETS",
				    "ExtraPublisher",
				    "extrapublisher");


  SVC_PERROR(pthread_barrier_init(&StartBarrier, 0, TNUM_FU + 1));
  InitSWBC();

  for (loop = 0; loop < TNUM_FU; loop++)
    {
      FCB *pfcb = &FcbTable[loop];
      SVC_PERROR(pthread_create(&th[loop], NULL, FuThread, pfcb));

      CPU_ZERO(&cpuset);
      CPU_SET(pfcb->core, &cpuset);
      SVC_PERROR(pthread_setaffinity_np(th[loop], sizeof(cpuset), &cpuset));
    }

  SVC_PERROR(pthread_barrier_wait(&StartBarrier));

  rclcpp::executors::MultiThreadedExecutor executor;
  executor.add_node(MS_node);
  executor.add_node(MP_node);
  executor.add_node(ES_node);
  executor.add_node(EP_node);

  executor.spin();

  printf("Shutting down ROS2 nodes gracefully...\n");
  rclcpp::shutdown();

  return 0;
}
