#include "systembuilder.h"

void
hw_sndmod(void){
	unsigned int a;
	unsigned int b;

	lprintf("hw_sndmod : Start!");

	while(1){
		HW_HW_BC_READ(&a);

		b = a + 1;

		HW_SW_BC_WRITE(b);
	}
}
