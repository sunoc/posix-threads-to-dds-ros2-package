#include "systembuilder.h"

void
sw_srmod(void){
	unsigned int snddata;
	unsigned int rcvdata;

	lprintf("sw_srmod  : Start Sw Send Recive Mod!");

	while(1){
		//トップからデータを受け取る
		SW_SW_BC_READ(&snddata);

		lprintf("sw_srmod  : Send to hard = 0x%x", snddata);

		SW_HW_BC_WRITE(snddata);
		HW_SW_BC_READ(&rcvdata);

		lprintf("sw_srmod  : Recive = 0x%x", rcvdata);
		SW_SW_BC2_WRITE(rcvdata);
	}
}
