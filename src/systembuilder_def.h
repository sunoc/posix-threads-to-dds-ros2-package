//////////////////////////////////////////////////////////////////////
// Copyright (c) 2005-2007 Shinya Honda (honda@ertl.jp)
//        Graduate School of Information Science, Nagoya Univ., JAPAN
//
// systembuilder_def.h
//
// 2023-11-18 12:06:09.690579
//
// SystemBuilder Standard-Def Header
//
//////////////////////////////////////////////////////////////////////

#ifndef SYSTEMBUILDER_DEF_H
#define SYSTEMBUILDER_DEF_H

#define __SYSB_SYSNAME_sp__
#define __SYSB_SYSNAME__ "sp"
#define OS_ITRON
#define SYSGEN_NEXTGENOS
#define __SYSB_SW_ITRON__
#define __SYSB_HW_FREQ_MHZ__ 100
#define __SYSB__
#define __SYSB_SW__
#define __SYSB_NUM_SWPE__  4

#endif
