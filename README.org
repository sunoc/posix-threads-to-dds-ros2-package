:PROPERTIES:
:ID:       98f74cf9-b942-4053-8fea-307bf99376fc
:END:
#+title: POSIX thread and ROS2 DDS interface
#+filetags: :draft:

* Integration into org-roam
The following command allows to put this README file into my org-roam system.
The properties at the top of the file are also here for that purpose.
#+BEGIN_SRC sh
  ln -s $PWD/README.org ~/Nextcloud/zettel/posix_thread_ros2_dds.org
#+END_SRC

#+RESULTS:

* Architecture
The figure [[fig:schematic]] below shows the architecture that is meant to be implemented. It
is to be noted that the grey arrows representing the communication between the
ROS2 nodes and the POSIX threads is not implemented yet. Only the deployment
using a single ~colcon~ command is done.

#+ATTR_LATEX: :width .7\textwidth
#+CAPTION: The implemented proof of concept for a communication between
#+CAPTION: a POSIX thread and ROS2 nodes.
#+NAME: fig:schematic
[[file:./img/schematic.png]]

* Test bench
Alternatively and on top of the presented architecture, a testing system was
implemented, meant to confirm the working of these test deployments of both the
ROS2 nodes and the pthreads communicating with each others, as seen in the
figure [[fig:tb]] below.
#+ATTR_LATEX: :width .7\textwidth
#+CAPTION: The proposed test bench implementation
#+NAME: fig:tb
[[file:./img/tb.png]]


The figure [[fig:uml]] below shows what the class and inheritance situation looks
like for the Nodes in the test bench.
#+ATTR_LATEX: :width .7\textwidth
#+CAPTION: A schematic of the class inheritance situation of the test bench
#+NAME: fig:uml
[[file:./img/uml.png]]

* Installation & running
The following steps will build and make the pthread system and its test bench available:
#+BEGIN_SRC sh
  rm -rf build/
  source /opt/ros/iron/setup.zsh # or .bash or else if you use another shell !
  colcon build --packages-select posix-threads-to-dds-ros2
  source ./install/setup.zsh # or .bash or else if you use another shell !
#+END_SRC

It is then possible to first run the test bench with this ~ros2~ command:
#+BEGIN_SRC sh
  ros2 run posix-threads-to-dds-ros2 testbench
#+END_SRC

And then, in a different terminal, this command to run the actual pthread system:
#+BEGIN_SRC sh
  ros2 run posix-threads-to-dds-ros2 pthreads
#+END_SRC
